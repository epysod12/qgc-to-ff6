#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
qgc-to-ff6.py
Ce script convertit un fichier QGroundControl (.plan) en plan de vol FreeFlight6 (.json).
Licence GPLv3
https://www.gnu.org/licenses/gpl-3.0.fr.html
2024/06/18
Jean-Charles Braun
'''

import os
import csv
import sys
import json
import pyproj
import colorama
import datetime
from colorama import Fore
from datetime import datetime

# Réinitialisation de la couleur après chaque 'print'
colorama.init(autoreset=True)

# Codes MAVLink récupérés sur "https://developer.parrot.com/docs/mavlink-flightplan/messages_v1.html"
MAV_CMD_NAV_WAYPOINT = 16                   # Navigate to waypoint.
MAV_CMD_NAV_RETURN_TO_LAUNCH = 20           # Return to home.
MAV_CMD_NAV_LAND = 21                       # Land.
MAV_CMD_NAV_TAKEOFF = 22                    # Take-off from ground / hand.
MAV_CMD_NAV_DELAY = 93                      # Delay the next navigation command. Same as MAV_CMD_CONDITION_DELAY.
MAV_CMD_CONDITION_DELAY = 112               # Delay the next navigation command. Same as MAV_CMD_NAV_DELAY.
MAV_CMD_DO_CHANGE_SPEED = 178               # Change speed.
MAV_CMD_DO_SET_ROI = 201                    # Set the region of interest (ROI) for the front camera.
MAV_CMD_DO_MOUNT_CONTROL = 205              # Control camera orientation.
MAV_CMD_DO_SET_CAM_TRIGG_DIST = 206         # Start/Stop GPS lapse.
MAV_CMD_DO_SET_CAM_TRIGG_INTERVAL = 214     # Start/Stop timelapse.
MAV_CMD_IMAGE_START_CAPTURE = 2000          # Start image capture sequence.
MAV_CMD_IMAGE_STOP_CAPTURE = 2001           # Stop image capture sequence.
MAV_CMD_VIDEO_START_CAPTURE = 2500          # Start video recording.
MAV_CMD_VIDEO_STOP_CAPTURE = 2501           # Stop video recording.
MAV_CMD_PANORAMA_CREATE = 2800              # Create a panorama at the current position.

# Codes MAVLink récupérés sur "https://mavlink.io/en/messages/common.html"
MAV_CMD_SET_CAMERA_MODE = 530               # Set camera running mode (CAMERA_MODE=2: image survey capture mode).
MAV_CMD_DO_GIMBAL_MANAGER_PITCHYAW = 1000   # Set gimbal manager pitch/yaw setpoints (low rate command).

def GeodesicData(startpoint, endpoint):
    uid1 = startpoint[0]
    uid2 = endpoint[0]
    lat1 = startpoint[1]
    lat2 = endpoint[1]
    lon1 = startpoint[2]
    lon2 = endpoint[2]
    geodesic = pyproj.Geod(ellps='WGS84')
    fwd_azimuth, bck_azimuth, distance = geodesic.inv(lon1, lat1, lon2, lat2)
    writerAZ.writerow([str(uid1) + '-' + str(uid2), round(fwd_azimuth, 3), round(bck_azimuth, 3), round(distance, 3)])

def convert_to_freeflight6(ff6_title, ff6_date, ff6_lat, ff6_lon):
    freeflight6_plan = {
        "version": 1,
        "title": ff6_title,
        "product": "ANAFI_4K",
        "productId": 2324,
        "uuid": ff6_title,
        "date": ff6_date,
        "progressive_course_activated": False,
        "dirty": False,
        "longitude": ff6_lon,
        "latitude": ff6_lat,
        "longitudeDelta": 0,
        "latitudeDelta": 0,
        "zoomLevel": 14,
        "rotation": 0,
        "tilt": 0,
        "mapType": 4,
        "plan": {
            "takeoff": [],
            "poi": [],
            "wayPoints": []
        }
    }

    cfgTilt = {
        "type": "Tilt",
        "angle": cameraPitch,
        "speed": 45
    }

    imgStart = {
        "type": "ImageStartCapture"
    }

    imgStop = {
        "type": "ImageStopCapture"
    }

    cfgLand = {
        "type": "Tilt",
        "angle": -cameraPitch,
        "speed": 45
    },{
        "type": "Landing"
    }

    for qgc_waypoint in Coord_WP:
        ff6_waypoint = {
            "latitude": qgc_waypoint[1],
            "longitude": qgc_waypoint[2],
            "altitude": qgc_waypoint[3],
            "yaw": -qgc_waypoint[4],
            "speed": surveySpeed,
            "continue": False,
            "followPOI": False,
            "follow": 1,
            "lastYaw": 0,
            "actions": []
        }

        if qgc_waypoint[5] == 'cfgTilt':
            ff6_waypoint["actions"].append(cfgTilt)

        elif qgc_waypoint[5] == 'imgStart':
            imgStart.update({"period": qgc_waypoint[6]})
            imgStart.update({"resolution": 13.600000381469727})
            imgStart.update({"nbOfPictures": 0})
            ff6_waypoint["actions"].append(imgStart)

        elif qgc_waypoint[5] == 'imgStop':
            ff6_waypoint["actions"].append(imgStop)

        elif qgc_waypoint[5] == 'cfgLand':
            ff6_waypoint["actions"].extend(cfgLand)

        freeflight6_plan["plan"]["wayPoints"].append(ff6_waypoint)

    with open(newPath + '/' + 'savedPlan.json', 'w') as ff6_file:
        json.dump(freeflight6_plan, ff6_file, indent=2)

# Récupération du nom du fichier et de son extension
document = os.path.abspath(sys.argv[1])
(pathname, fullname) = os.path.split(document)
(filename, extension) = os.path.splitext(fullname)

# Ouverture du fichier JSON
qgcFP = open(document, 'r')
plan = json.load(qgcFP)

# Traitement du fichier .plan s'il est de type "survey"
for datum in plan['mission']['items']:
    if datum.get('complexItemType') == 'survey':

        # Récupération des variables de la section MISSION
        hoverSpeed = plan['mission']['hoverSpeed']
        plannedHomePosition = plan['mission']['plannedHomePosition'][0:2]

        # Récupération des variables de la section ITEMS
        CameraShots = datum['TransectStyleComplexItem']['CameraShots']
        CameraTriggerInTurnAround = datum['TransectStyleComplexItem']['CameraTriggerInTurnAround']
        HoverAndCapture = datum['TransectStyleComplexItem']['HoverAndCapture']
        Refly90Degrees = datum['TransectStyleComplexItem']['Refly90Degrees']
        TurnAroundDistance = datum['TransectStyleComplexItem']['TurnAroundDistance']
        FrontalOverlap = datum['TransectStyleComplexItem']['CameraCalc']['FrontalOverlap']
        SideOverlap = datum['TransectStyleComplexItem']['CameraCalc']['SideOverlap']
        angle = datum['angle']
        polygon = datum['polygon']

        # Si la mission est de type "Hover and Capture", le script s'arrête
        if HoverAndCapture == True:
            print(Fore.YELLOW + 'Warning!' + Fore.RESET + ' QGC FlightPlan is ' + Fore.YELLOW + 'HoverAndCapture' + Fore.RESET + '...')
            print(Fore.YELLOW + 'Program is stopped' + Fore.RESET + '...')
            break

        # Initialisation de la liste contenant les informations des WayPoints
        Coord_WP = []

        # Définition du Pitch Camera par défaut
        cameraPitch = -90

        # Définition de la vitesse du drone
        surveySpeed = hoverSpeed

        # Création du dossier de destination
        newPath = pathname + '/' + filename
        if not os.path.exists(newPath):
            os.mkdir(newPath)

        # Création du fichier CSV contenant les commandes MAVLink
        with open(newPath + '/' + 'uavMavCmd.csv', 'w') as uavMavCmd:
            writerMC = csv.writer(uavMavCmd, delimiter='\t')

            # Récupération des informations pour préparer le Décollage / Takeoff
            for record in plan['mission']['items']:
                if record.get('command') == 530:
                    cmd = record['command']
                    uid = record['doJumpId']
                    rsv = record['params'][0]
                    mod = record['params'][1]

                    data = [cmd, uid, rsv, mod]
                    writerMC.writerow(data)

                if record.get('command') == 205:
                    cmd = record['command']
                    uid = record['doJumpId']
                    cameraPitch = record['params'][0]
                    rol = record['params'][1]
                    yaw = record['params'][2]
                    alt = record['params'][3]
                    lat = record['params'][4]
                    lon = record['params'][5]
                    mod = record['params'][6]

                    data = [cmd, uid, cameraPitch, rol, yaw]
                    writerMC.writerow(data)

                if record.get('command') == 178:
                    cmd = record['command']
                    uid = record['doJumpId']
                    typ = record['params'][0]
                    surveySpeed = record['params'][1]
                    thr = record['params'][2]

                    data = [cmd, uid, typ, surveySpeed, thr]
                    writerMC.writerow(data)

                if record.get('command') == 22:
                    surveyAltitude = record['Altitude']
                    cmd = record['command']
                    uid = record['doJumpId']
                    pit = record['params'][0]
                    yaw = record['params'][3]
                    lat = record['params'][4]
                    lon = record['params'][5]
                    alt = record['params'][6]

                    data = [cmd, uid, lat, lon, alt]
                    writerMC.writerow(data)

                    takeOffPosition = [lat, lon, alt]
                    Coord_WP.append(['TKF'] + takeOffPosition)

            # Récupération et enregistrement des données du plan de vol
            for sub_item in datum['TransectStyleComplexItem']['Items']:
                if sub_item.get('command') == 16:
                    cmd = sub_item['command']
                    uid = sub_item['doJumpId']
                    yaw = sub_item['params'][3]
                    lat = sub_item['params'][4]
                    lon = sub_item['params'][5]
                    alt = sub_item['params'][6]

                    data = [cmd, uid, lat, lon, alt]
                    writerMC.writerow(data)

                    Coord_WP.append(['wp'+str(uid), lat, lon, alt])

                if sub_item.get('command') == 206:
                    cmd = sub_item['command']
                    uid = sub_item['doJumpId']
                    triggerDistance = sub_item['params'][0]
                    sht = sub_item['params'][1]
                    trg = sub_item['params'][2]

                    data = [cmd, uid, triggerDistance, sht, trg]
                    writerMC.writerow(data)

                    Coord_WP[-1].append(round(triggerDistance/surveySpeed, 1))

            # Récupération des données du point d'Atterrissage / Landing
            for record in plan['mission']['items']:
                if record.get('command') == 20:
                    cmd = record['command']
                    uid = record['doJumpId']

                    data = [cmd, uid]
                    writerMC.writerow(data)

                    plannedHomePosition.append(surveyAltitude)
                    Coord_WP.append(['HOME'] + plannedHomePosition)

        # Si la vitesse (en m/s) n'est pas une valeure entière, le script s'arrête
        if float(surveySpeed).is_integer() == False:
            print(Fore.YELLOW + 'Warning!' + Fore.RESET + ' UAV Speed is ' + Fore.YELLOW + 'NOT Integer Value' + Fore.RESET + '...')
            print(Fore.YELLOW + 'Program is stopped' + Fore.RESET + '...')
            break

        # Création du fichier CSV contenant les azimuts et les distances
        with open(newPath + '/' + 'survAzDst.csv', 'w') as survAzDst:
            writerAZ = csv.writer(survAzDst, delimiter=',')
            for num in range(len(Coord_WP))[:-1]:
                GeodesicData(Coord_WP[num], Coord_WP[num + 1])

        # Ajout des azimuts et des actions à effectuer par le drone dans la variable Coord_WP
        with open(newPath + '/' + 'survAzDst.csv', 'r') as survAzDst:
            readerAZ = csv.reader(survAzDst, delimiter=',')

            if TurnAroundDistance == 0:
                for num, line in enumerate(readerAZ):
                    if num == 0:
                        Coord_WP[num].insert(4, float(line[1]))
                        Coord_WP[num].insert(5, 'cfgTilt')
                    elif num%2 != 0:
                        Coord_WP[num].insert(4, float(line[1]))
                        Coord_WP[num].insert(5, 'imgStart')
                    elif num%2 == 0:
                        Coord_WP[num].insert(4, float(line[1]))
                        Coord_WP[num].insert(5, 'imgStop')
                Coord_WP[num + 1].insert(4, angle)
                Coord_WP[num + 1].insert(5, 'cfgLand')

            if TurnAroundDistance > 0:
                list_AzDst = list(readerAZ)
                Coord_WP[0].insert(4, float(list_AzDst[0][1]))
                Coord_WP[0].insert(5, 'cfgTilt')
                for num in range(2, len(list_AzDst), 4):
                    Coord_WP[num - 1].insert(4, float(list_AzDst[num - 1][1]))
                    Coord_WP[num - 1].insert(5, '')
                    Coord_WP[num].insert(4, float(list_AzDst[num][1]))
                    Coord_WP[num].insert(5, 'imgStart')
                    Coord_WP[num + 1].insert(4, float(list_AzDst[num + 1][1]))
                    Coord_WP[num + 1].insert(5, 'imgStop')
                    Coord_WP[num + 2].insert(4, float(list_AzDst[num + 2][1]))
                    Coord_WP[num + 2].insert(5, '')
                Coord_WP[-1].insert(4, angle)
                Coord_WP[-1].insert(5, 'cfgLand')

        # Si le Pitch Camera est -90°, le drone garde une orientation fixe pour toute la mission
        if cameraPitch == -90:
            for line in Coord_WP:
                line[4] = angle

        # Création du fichier CSV contenant les coordonnées des WayPoints
        with open(newPath + '/' + 'wayPoints.csv', 'w') as wayPoints:
            writerWP = csv.writer(wayPoints, delimiter=',')
            for entry in Coord_WP:
                writerWP.writerow(entry)

        # Récupération de la date au format Unix, en milliseconde
        UnixTime = int(datetime.now().timestamp() * 1000)

        # Calcul du Centroïde d'après les coordonnées du Polygone
        list_PolyLat, list_PolyLon = [], []
        for point in polygon:
            list_PolyLat.append(point[0])
            list_PolyLon.append(point[1])
        CentroidLat = (sum(list_PolyLat) / len(polygon))
        CentroidLon = (sum(list_PolyLon) / len(polygon))

        # Lancement du processus
        convert_to_freeflight6(filename, UnixTime, CentroidLat, CentroidLon)
        print(Fore.GREEN + 'Success!' + Fore.RESET + ' FF6 FlightPlan created in ' + Fore.GREEN + filename + Fore.RESET + ' folder.')
        print(Fore.GREEN + 'savedPlan.json' + Fore.RESET + ' must be checked...')

    elif datum.get('complexItemType') == 'CorridorScan':
        print(Fore.YELLOW + 'Warning!' + Fore.RESET + ' QGC FlightPlan is ' + Fore.YELLOW + 'CorridorScan' + Fore.RESET + '...')
        print(Fore.YELLOW + 'Program is stopped' + Fore.RESET + '...')
        break

    elif datum.get('complexItemType') == 'StructureScan':
        print(Fore.YELLOW + 'Warning!' + Fore.RESET + ' QGC FlightPlan is ' + Fore.YELLOW + 'StructureScan' + Fore.RESET + '...')
        print(Fore.YELLOW + 'Program is stopped' + Fore.RESET + '...')
        break

# Fermeture du fichier JSON
qgcFP.close()
