=============
qgc-to-ff6.py
=============

Introduction
------------

Ce script convertit un fichier ``QGroundControl`` (.plan) en plan de vol ``FreeFlight6`` (.json).

Prérequis
---------

- python3-pyproj
- python3-colorama

Utilisation
-----------

| L'exécution du script nécessite un seul argument : le fichier ``QGroundControl`` (.plan).
| Le lancement s'effectue dans un terminal :

    ``$ python3 qgc-to-ff6.py /home/<...>/mon-plan-de-vol.plan``

Fonctionnement
--------------

Le script récupère les coordonnées des WayPoints, ainsi que d'autres caractéristiques du plan de vol QGroundControl comme l'altitude, la vitesse, le *pitch camera*, etc.

Les données sont ensuite remaniées pour être lisibles par FreeFlight6 dans son mode de pilotage ``Flight Plan``.

Nous obtenons alors 3 fichiers CSV + 1 fichier JSON :

#. | **wayPoints.csv**
   | -> les coordonnées des points de repères + diverses informations
#. | **uavMavCmd.csv**
   | -> la liste des commandes MAVLink
#. | **survAzDst.csv**
   | -> les azimuts et les distances calculées entre les WayPoints
#. | **savedPlan.json**
   | -> le plan de vol FreeFlight6, au format JSON

Licence
-------

| Le programme ci-joint est publié sous licence GPLv3.
| https://www.gnu.org/licenses/gpl-3.0.fr.html
